# Module 01: DevOps Introduction
Welcome to the DevOps Bootcamp Sydney! 
This is the first module. This module is designed to give you a good understanding of what is DevOps and build a study path.

# OBJECTIVES
 * Better understand the philoshopy of DevOps 
 * Get to know DevOps processess
 * Familiar yourself with crucial technologies
 * Build your home lab


# [LESSON 1: What is DevOps?](LESSON-1.md)

TBD


# [LESSON 2: DevOps Processes](LESSON-2.md)

TBD

# [LESSON 3: Technologies!](LESSON-3.md)

TBD

# Videos

[AWS re:Invent 2015 | (ISM301) Engineering Netflix Global Operations in the Cloud / Youtube](https://youtu.be/IkPb15FfuQU)